package com.app.optuspoc.ui.testsuite

import org.junit.runner.RunWith
import org.junit.runners.Suite
import com.app.optuspoc.ui.view.AlbumDetailFragmentTest
import com.app.optuspoc.ui.view.AlbumFragmentTest
import com.app.optuspoc.ui.view.UserFragmentTest


@RunWith(Suite::class)
@Suite.SuiteClasses(
    UserFragmentTest::class,
    AlbumFragmentTest::class,
    AlbumDetailFragmentTest::class
)
class MainSuite {
}