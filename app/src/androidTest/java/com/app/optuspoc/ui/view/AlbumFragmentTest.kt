package com.app.optuspoc.ui.view

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import com.app.optuspoc.R
import com.app.optuspoc.base.MainActivity
import com.app.optuspoc.ui.adapter.album.AlbumViewHolder
import com.app.optuspoc.ui.adapter.user.UserViewHolder
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class AlbumFragmentTest {

    @get: Rule
    val activityRule: ActivityTestRule<MainActivity> =
        ActivityTestRule(MainActivity::class.java, false, false)

    private val listItemPosition = 5
    private val title = "natus nisi omnis corporis facere molestiae rerum in"

    @Before
    fun setUp() {
        val intent = Intent()
        activityRule.launchActivity(intent)
    }

    @Test
    fun testFragmentAlbumProgressBarIsDisplayed() {

        onView(withId(R.id.recyclerViewUser))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<UserViewHolder>(
                    listItemPosition,
                    ViewActions.click()
                )
            )

        IdlingResource.ResourceCallback {
            onView(withId(R.id.progressBarUser))
                .check(matches(isDisplayed()))
        }
    }

    @Test
    fun testAlbumFragmentIsDisplayed() {
        onView(withId(R.id.recyclerViewUser))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<UserViewHolder>(
                    listItemPosition,
                    ViewActions.click()
                )
            )

        onView(withId(R.id.recyclerViewAlbum))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testRecyclerViewFragmentAlbumTestScrollingToPosition() {
        onView(withId(R.id.recyclerViewUser))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<UserViewHolder>(
                    listItemPosition,
                    ViewActions.click()
                )
            )
        onView(withId(R.id.recyclerViewAlbum))
            .perform(RecyclerViewActions.scrollToPosition<UserViewHolder>(listItemPosition))
    }

    @Test
    fun testRecyclerViewFragmentAlbumHasTextViewUserName() {
        IdlingResource.ResourceCallback {
            onView(withId(R.id.recyclerViewUser))
                .perform(
                    RecyclerViewActions.actionOnItemAtPosition<UserViewHolder>(
                        listItemPosition,
                        ViewActions.click()
                    )
                )
            onView(withId(R.id.recyclerViewAlbum))
                .perform(RecyclerViewActions.scrollTo<AlbumViewHolder>(ViewMatchers.withText(title)))
                .check(matches(ViewMatchers.hasDescendant(withId(R.id.textViewAlbumTitle))))
        }
    }

    @Test
    fun testRecyclerviewAlbumOnClickItem() {
        onView(withId(R.id.recyclerViewUser))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<UserViewHolder>(
                    listItemPosition,
                    ViewActions.click()
                )
            )
        onView(withId(R.id.recyclerViewAlbum))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<AlbumViewHolder>(
                    listItemPosition,
                    ViewActions.click()
                )
            )
    }
}