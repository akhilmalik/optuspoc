package com.app.optuspoc.ui.view


import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import com.app.optuspoc.R
import com.app.optuspoc.base.MainActivity
import com.app.optuspoc.ui.adapter.user.UserViewHolder
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class UserFragmentTest {

    @get: Rule
    val activityRule: ActivityTestRule<MainActivity> =
        ActivityTestRule(MainActivity::class.java, false, false)

    private val listItemPosition = 5
    private val intent = Intent()
    private val id = "ID : 5"
    private val name = "Name : Chelsey Dietrich"
    private val email = "Email : Lucio_Hettinger@annie.ca"
    private val phone = "Phone : (254)954-1289"

    @Before
    fun setUp() {
        activityRule.launchActivity(intent)
    }

    @Test
    fun appLaunchSuccessfully() {
        ActivityScenario.launch(MainActivity::class.java)
    }

    @Test
    fun checkUserFragmentIsDisplayed() {
        val fragmentUser = UserFragment()
        activityRule.activity.supportFragmentManager.beginTransaction()
            .add(R.id.fragment, fragmentUser).commit()
    }

    @Test
    fun recyclerViewFragmentUserIsDisplayed() {
        onView(withId(R.id.recyclerViewUser)).check(matches(isDisplayed()))
    }

    @Test
    fun onLaunchCheckProgressBarIsDisplayed() {
        IdlingResource.ResourceCallback {
            onView(withId(R.id.progressBarUser))
                .check(matches(isDisplayed()))
        }
    }

    @Test
    fun testRecyclerViewFragmentUserHasTextViewUserID() {
        IdlingResource.ResourceCallback {
            onView(withId(R.id.recyclerViewUser))
                .check(matches(hasDescendant(withId(R.id.textViewUserID))))
                .check(matches(withText(name)))
        }
    }

    @Test
    fun testRecyclerViewFragmentUserHasTextViewUserName() {
        IdlingResource.ResourceCallback {
            onView(withId(R.id.recyclerViewUser))
                .check(matches(hasDescendant(withId(R.id.textViewUserName))))
                .check(matches(withText(name)))
        }
    }

    @Test
    fun testRecyclerViewFragmentUserHasTextViewUserEmail() {
        IdlingResource.ResourceCallback {
            onView(withId(R.id.recyclerViewUser))
                .check(matches(hasDescendant(withId(R.id.textViewUserEmail))))
                .perform(actionOnItemAtPosition<UserViewHolder>(listItemPosition, typeText(email)))
        }
    }

    @Test
    fun testRecyclerViewFragmentUserHasTextViewUserPhone() {
        IdlingResource.ResourceCallback {
            onView(withId(R.id.recyclerViewUser))
                .check(matches(hasDescendant(withId(R.id.textViewUserPhone))))
                .check(matches(withText(phone)))
        }
    }

    @Test
    fun testRecyclerViewFragmentUserTestScrollToPosition() {
        onView(withId(R.id.recyclerViewUser))
            .perform(RecyclerViewActions.scrollToPosition<UserViewHolder>(listItemPosition))
    }

    @Test
    fun testRecyclerviewUserOnClickItem() {
        onView(withId(R.id.recyclerViewUser))
            .perform(actionOnItemAtPosition<UserViewHolder>(listItemPosition, click()))
    }
}