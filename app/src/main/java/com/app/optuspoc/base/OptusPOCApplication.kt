package com.app.optuspoc.base

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class OptusPOCApplication :Application() {
}