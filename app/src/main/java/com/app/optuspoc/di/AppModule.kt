package com.app.optuspoc.di

import com.app.optuspoc.network.APIBuilder
import com.app.optuspoc.network.APIServiceImpl
import com.app.optuspoc.ui.factory.AlbumViewModelFactory
import com.app.optuspoc.ui.factory.UserViewModelFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun getUserViewModelFactory(): UserViewModelFactory {
        return UserViewModelFactory(APIServiceImpl(APIBuilder.apiService))
    }

    @Singleton
    @Provides
    fun getAlbumViewModelFactory(): AlbumViewModelFactory {
        return AlbumViewModelFactory(APIServiceImpl(APIBuilder.apiService))
    }


}