package com.app.optuspoc.repository

import com.app.optuspoc.model.AlbumModel

/**
 * Album Repository
 */
interface AlbumRepository {
  suspend fun getAlbumApi(id: Int) : List<AlbumModel>
}