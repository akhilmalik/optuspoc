package com.app.optuspoc.repository

import com.app.optuspoc.model.UserModel

/**
 * User Repository
 */
interface UserRepository {
    suspend fun getUsers(): List<UserModel>
}