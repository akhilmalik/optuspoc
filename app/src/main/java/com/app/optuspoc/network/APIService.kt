package com.app.optuspoc.network

import com.app.optuspoc.model.AlbumModel
import com.app.optuspoc.model.UserModel
import retrofit2.http.GET
import retrofit2.http.Query


interface APIService {

    @GET("users")
    suspend fun getUsers(): List<UserModel>

    @GET("photos")
    suspend fun getAlbums(@Query("albumId") id: Int): List<AlbumModel>

}