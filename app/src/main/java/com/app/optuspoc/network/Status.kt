package com.app.optuspoc.network


/**
 * @enum_class{Status}
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}