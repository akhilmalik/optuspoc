package com.app.optuspoc.network

import com.app.optuspoc.util.Constants.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Network builder class
 */
object APIBuilder {

    private fun getNetworkInstance(): Retrofit {

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val apiService: APIService = getNetworkInstance().create(APIService::class.java)
}