package com.app.optuspoc.network

import com.app.optuspoc.model.AlbumModel
import com.app.optuspoc.model.UserModel
import com.app.optuspoc.repository.AlbumRepository
import com.app.optuspoc.repository.UserRepository

class APIServiceImpl(private val apiService: APIService) : UserRepository, AlbumRepository {

    override suspend fun getUsers(): List<UserModel> = apiService.getUsers()

    override suspend fun getAlbumApi(id: Int): List<AlbumModel> = apiService.getAlbums(id)

}