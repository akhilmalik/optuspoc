package com.app.optuspoc.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



data class AlbumModel(
    @SerializedName("albumId")
    @Expose
    val albumId: Int,

    @SerializedName("id")
    @Expose
    val id: Int,

    @SerializedName("thumbnailUrl")
    @Expose
    val thumbnailUrl: String,

    @SerializedName("title")
    @Expose
    val title: String,

    @SerializedName("url")
    @Expose
    val url: String
)