package com.app.optuspoc.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.optuspoc.model.AlbumModel
import com.app.optuspoc.repository.AlbumRepository
import com.app.optuspoc.network.Result
import com.app.optuspoc.util.Constants.ERROR_MSG
import kotlinx.coroutines.launch

/**
 * @class{AlbumViewModel}
 */
class AlbumViewModel(private val albumRepo: AlbumRepository) : ViewModel() {

    private val albumList = MutableLiveData<Result<List<AlbumModel>>>()

    /**
     * fetch list of albums
     */
    fun getAlbumList(id: Int): LiveData<Result<List<AlbumModel>>> {
        return getAlbum(id)
    }

    private fun getAlbum(id: Int): LiveData<Result<List<AlbumModel>>> {
        viewModelScope.launch {
            albumList.postValue(Result.loading(null))
            try {
                albumList.postValue(Result.success(albumRepo.getAlbumApi(id)))
            } catch (exception: Exception) {
                albumList.postValue(Result.error(ERROR_MSG, null))
                exception.message ?: "Error! $ERROR_MSG"
            }
        }
        return albumList
    }
}