package com.app.optuspoc.ui.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.optuspoc.repository.AlbumRepository
import com.app.optuspoc.ui.viewmodel.AlbumViewModel
import javax.inject.Inject


class AlbumViewModelFactory @Inject constructor(private val albumRepository: AlbumRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AlbumViewModel(albumRepository) as T
    }
}