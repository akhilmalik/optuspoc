package com.app.optuspoc.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.app.optuspoc.base.BaseFragment
import com.app.optuspoc.R
import com.app.optuspoc.base.showSnackBar
import com.app.optuspoc.model.AlbumModel
import com.app.optuspoc.databinding.FragmentAlbumBinding
import com.app.optuspoc.network.Status
import com.app.optuspoc.ui.adapter.album.AlbumAdapter
import com.app.optuspoc.ui.factory.AlbumViewModelFactory
import com.app.optuspoc.ui.viewmodel.AlbumViewModel
import com.app.optuspoc.util.Constants.ALBUM_ID
import com.app.optuspoc.util.Constants.ERROR_MSG
import com.app.optuspoc.util.Constants.ID
import com.app.optuspoc.util.Constants.NO_CONNECTION
import com.app.optuspoc.util.Constants.PHOTO_ID
import com.app.optuspoc.util.Constants.TITLE
import com.app.optuspoc.util.Constants.URL
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class AlbumFragment : BaseFragment(), AlbumAdapter.OnImageClickListener {

    private lateinit var albumViewModel: AlbumViewModel
    private lateinit var binding: FragmentAlbumBinding
    @Inject
    lateinit var albumFactory: AlbumViewModelFactory
    private var userID = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userID = arguments?.getInt(ID)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAlbumBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.mToolbarAlbumTitle.text = "Album ID: $userID"
        albumViewModel = ViewModelProviders.of(this@AlbumFragment, albumFactory).get(
            AlbumViewModel::class.java
        )

        /**
         * check connectivity
         */
        if (isConnection()!!) {
            setUpAPICall()
        } else {
            binding.recyclerViewAlbum.showSnackBar(NO_CONNECTION)
        }

    }

    /**
     * setup view-model and fetch data for album and display it in recyclerview
     **/
    private fun setUpAPICall() {

        albumViewModel.getAlbumList(userID).observe(viewLifecycleOwner, Observer {
            it.let { result ->
                when (result.status) {

                    Status.LOADING -> {
                        binding.progressBarAlbum.visibility = View.VISIBLE
                        binding.recyclerViewAlbum.visibility = View.GONE

                    }

                    Status.SUCCESS -> {
                        binding.recyclerViewAlbum.visibility = View.VISIBLE
                        binding.progressBarAlbum.visibility = View.GONE
                        result.data.let { album ->
                            binding.recyclerViewAlbum.setHasFixedSize(true)
                            binding.recyclerViewAlbum.adapter =
                                album?.let { albumList -> AlbumAdapter(albumList, this) }
                        }
                    }

                    Status.ERROR -> {
                        binding.recyclerViewAlbum.visibility = View.VISIBLE
                        binding.progressBarAlbum.visibility = View.GONE
                        binding.recyclerViewAlbum.showSnackBar(ERROR_MSG)
                    }
                }

            }
        })
    }


    /**
     * on item click of recyclerview
     */
    override fun onItemClick(item: AlbumModel?) {

        val bundle = bundleOf(
            ALBUM_ID to item?.albumId,
            PHOTO_ID to item?.id,
            TITLE to item?.title,
            URL to item?.url
        )

        findNavController().navigate(R.id.action_fragmentAlbum_to_fragmentAlbumDetails, bundle)
    }


}
