package com.app.optuspoc.ui.adapter.user

import androidx.recyclerview.widget.RecyclerView
import com.app.optuspoc.databinding.ItemUserBinding


class UserViewHolder(val itemUserBinding: ItemUserBinding): RecyclerView.ViewHolder(itemUserBinding.root)