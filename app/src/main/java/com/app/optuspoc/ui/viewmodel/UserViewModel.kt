package com.app.optuspoc.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.optuspoc.model.UserModel
import com.app.optuspoc.repository.UserRepository
import com.app.optuspoc.network.Result
import com.app.optuspoc.util.Constants.ERROR_MSG
import kotlinx.coroutines.launch

/**
 * @class{UserViewModel}
 */
class UserViewModel(private val userRepo: UserRepository) : ViewModel() {
    private val usersList = MutableLiveData<Result<List<UserModel>>>()

    init {
        getUsersList()
    }

    /**
     * fetch list of users
     */
    private fun getUsersList() {
        viewModelScope.launch {
            usersList.postValue(Result.loading(null))
            try {
                usersList.postValue(Result.success(userRepo.getUsers()))
            } catch (exception: Exception) {
                usersList.postValue(Result.error(ERROR_MSG, null))
                exception.message ?: "Error! $ERROR_MSG"
            }
        }
    }

    fun getUserList(): LiveData<Result<List<UserModel>>> {
        return usersList
    }
}