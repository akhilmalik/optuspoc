package com.app.optuspoc.ui.adapter.album

import androidx.recyclerview.widget.RecyclerView
import com.app.optuspoc.databinding.ItemAlbumBinding


class AlbumViewHolder(val itemAlbumBinding: ItemAlbumBinding) :
    RecyclerView.ViewHolder(itemAlbumBinding.root)