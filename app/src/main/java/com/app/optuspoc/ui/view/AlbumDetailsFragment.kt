package com.app.optuspoc.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.optuspoc.base.BaseFragment
import com.app.optuspoc.databinding.FragmentAlbumDetailsBinding
import com.app.optuspoc.util.Constants.ALBUM_ID
import com.app.optuspoc.util.Constants.PHOTO_ID
import com.app.optuspoc.util.Constants.TITLE
import com.app.optuspoc.util.Constants.URL
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.header.view.*


class AlbumDetailsFragment : BaseFragment() {

    private lateinit var binding: FragmentAlbumDetailsBinding
    private var albumID = 0
    private var photoID = 0
    private var title = ""
    private var url = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAlbumDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        albumID = arguments?.getInt(ALBUM_ID)!!
        photoID = arguments?.getInt(PHOTO_ID)!!
        title = arguments?.getString(TITLE)!!
        url = arguments?.getString(URL)!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.toolBarHeaderView.textViewAlbumDetailID.text = "Album ID : $albumID"
        binding.toolBarHeaderView.textViewPhotoDetailID.text = "Photo ID : $photoID"
        binding.textViewAlbumDetailTitle.text = title
        Picasso.get().load(url).into(binding.imageViewAlbumImage)
    }
}