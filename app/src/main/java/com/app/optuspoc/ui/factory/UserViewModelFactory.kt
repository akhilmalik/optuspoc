package com.app.optuspoc.ui.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.optuspoc.repository.UserRepository
import com.app.optuspoc.ui.viewmodel.UserViewModel
import javax.inject.Inject

class UserViewModelFactory @Inject constructor(private val userRepository: UserRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UserViewModel(userRepository) as T
    }
}
