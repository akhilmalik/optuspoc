package com.app.optuspoc.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.app.optuspoc.R
import com.app.optuspoc.base.BaseFragment
import com.app.optuspoc.base.showSnackBar
import com.app.optuspoc.model.UserModel
import com.app.optuspoc.databinding.FragmentUserBinding
import com.app.optuspoc.network.Status
import com.app.optuspoc.ui.adapter.user.UserAdapter
import com.app.optuspoc.ui.factory.UserViewModelFactory
import com.app.optuspoc.ui.viewmodel.UserViewModel
import com.app.optuspoc.util.Constants.ERROR_MSG
import com.app.optuspoc.util.Constants.ID
import com.app.optuspoc.util.Constants.NO_CONNECTION
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


/**
 * @fragment{FragmentUser}
 */
@AndroidEntryPoint
class UserFragment : BaseFragment(), UserAdapter.OnItemClickListener {

    private lateinit var userViewModel: UserViewModel
    private lateinit var binding: FragmentUserBinding
    @Inject
    lateinit var userFactory: UserViewModelFactory


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val title: String = getString(R.string.user_info)
        binding.mToolbarUserTitle.text = title
        userViewModel =
            ViewModelProviders.of(this@UserFragment, userFactory)
                .get(UserViewModel::class.java)

        /**
         * check connectivity
         */
        if (isConnection()) {
            setUpAPICall()
        } else {
            binding.recyclerViewUser.showSnackBar(NO_CONNECTION)
        }
    }

    /**
     * setup view-model and fetch data for user and display it in recyclerview
     **/
    private fun setUpAPICall() {
        userViewModel.getUserList().observe(viewLifecycleOwner, Observer {
            it.let { result ->
                when (result.status) {

                    Status.LOADING -> {
                        binding.progressBarUser.visibility = View.VISIBLE
                        binding.recyclerViewUser.visibility = View.GONE
                    }

                    Status.SUCCESS -> {
                        binding.recyclerViewUser.visibility = View.VISIBLE
                        binding.progressBarUser.visibility = View.GONE/**/
                        result.data.let { user ->
                            binding.recyclerViewUser.setHasFixedSize(true)
                            binding.recyclerViewUser.adapter =
                                user?.let { userList ->
                                    UserAdapter(
                                        userList,
                                        this
                                    )
                                }
                        }
                    }

                    Status.ERROR -> {
                        binding.recyclerViewUser.visibility = View.VISIBLE
                        binding.progressBarUser.visibility = View.GONE
                        binding.recyclerViewUser.showSnackBar(ERROR_MSG)
                    }
                }
            }
        })
    }

    /**
     * on item click of recyclerview
     */
    override fun onItemClick(item: UserModel?) {
        val bundle = bundleOf(ID to item?.id)
        findNavController().navigate(R.id.action_fragmentUser_to_fragmentAlbum, bundle)
    }
}
