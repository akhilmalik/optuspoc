package com.app.optuspoc.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.app.optuspoc.model.UserModel
import com.app.optuspoc.repository.UserRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import com.app.optuspoc.network.Result
import com.app.optuspoc.util.Constants.ERROR_MSG


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class UserViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var userRepository: UserRepository

    @Mock
    private lateinit var userObserver: Observer<Result<List<UserModel>>>

    @Test
    fun `test given when user response is success`() {

        testCoroutineRule.runBlockingTest {
            doReturn(emptyList<UserModel>())
                .`when`(userRepository).getUsers()

            val userViewModel = UserViewModel(userRepository)
            userViewModel.getUserList().observeForever(userObserver)
            verify(userRepository).getUsers()
            verify(userObserver).onChanged(Result.success(emptyList()))
            userViewModel.getUserList().removeObserver(userObserver)
        }
    }


    @Test
    fun `test given when user response is failure`() {
        testCoroutineRule.runBlockingTest {
            doThrow(RuntimeException(ERROR_MSG))
                .`when`(userRepository)
                .getUsers()

            val userViewModel = UserViewModel(userRepository)
            userViewModel.getUserList().observeForever(userObserver)
            verify(userRepository).getUsers()
            verify(userObserver).onChanged(Result.error(ERROR_MSG , null))
            userViewModel.getUserList().removeObserver(userObserver)
        }
    }

}
